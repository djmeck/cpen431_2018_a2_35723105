package com.s35723105.CPEN431.A2.CPEN431_A2;

import ca.NetSysLab.ProtocolBuffers.KeyValueRequest.KVRequest;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse;
import ca.NetSysLab.ProtocolBuffers.Message;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;

import java.io.IOException;
import java.net.*;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.s35723105.CPEN431.A2.CPEN431_A2.Utilities.concatenateByteArrays;
import static com.s35723105.CPEN431.A2.CPEN431_A2.Utilities.makeChecksum;
import static com.s35723105.CPEN431.A2.CPEN431_A2.Utilities.validateChecksum;

public class UdpClient {

    private final static Logger LOGGER = Logger.getLogger(UdpClient.class.getName());

    DatagramSocket socket;

    private final InetAddress destinationAddress;
    private final Integer destinationPort;

    private final Integer sourcePort;

    private final Integer timeoutMilliseconds;

    /**
     * Construct a udpClient. Destination port and ip are specified in constructor, this
     * constructor is suitable for a client node.
     *
     * @param destinationPort port of host
     * @param destinationIp ipv4 address of host
     * @throws IOException if provided ip address is invalid
     */
    UdpClient(Integer destinationPort, String destinationIp) throws IOException {
        LOGGER.log(Level.INFO, "Creating a updClient for a client node.");
        this.destinationAddress = InetAddress.getByName(destinationIp);
        this.destinationPort = destinationPort;
        this.timeoutMilliseconds = 100;
        this.socket = new DatagramSocket();

        this.sourcePort = null;
    }

    /**
     * Construct a udpClient. Destination port and ip are specified in constructor, this
     * constructor is suitable for a client node.
     *
     * @param destinationPort port of host
     * @param destinationIp ipv4 address of host
     * @param timeoutMilliseconds initial waiting period before timeout
     * @throws IOException if provided ip address is invalid
     */
    UdpClient(Integer destinationPort, String destinationIp, Integer timeoutMilliseconds) throws IOException {
        LOGGER.log(Level.INFO, "Creating a updClient for a client node.");
        this.destinationAddress = InetAddress.getByName(destinationIp);
        this.destinationPort = destinationPort;
        this.timeoutMilliseconds = timeoutMilliseconds;
        this.socket = new DatagramSocket();

        this.sourcePort = null;
    }

    /**
     * Construct a udpClient for a server. Destination port and address can be specified
     * at send time. Suitable for a server node.
     *
     * @param sourcePort port to listen on
     * @throws IOException if socket cannot be established with provided source port
     */
    UdpClient(Integer sourcePort) throws IOException {
        LOGGER.log(Level.INFO, "Creating a updClient for a server node");
        this.sourcePort = sourcePort;
        this.socket = new DatagramSocket(this.sourcePort);

        this.destinationAddress = null;
        this.destinationPort = null;
        this.timeoutMilliseconds = null;
    }

    /**
     * Method will listen on the port specified in the constructor until a packet is received.
     * If a packet is received and checksum validated, it will be returned to the callee.
     *
     * @param noPuts if puts should be ignored
     * @return the message received, including messageId and payload
     */
    public RequestInfo listen(Boolean noPuts) throws IOException {
        while(true) {

            byte[] receiveBuffer = new byte[Constants.RECEIVE_BUFFER_LENGTH];
            DatagramPacket packet = new DatagramPacket(receiveBuffer, receiveBuffer.length);

            Message.Msg message = null;

            try {
                this.socket.receive(packet);

                message = Message.Msg.parseFrom(extractPacketBytes(packet));
                KVRequest kvRequest = KVRequest.parseFrom(message.getPayload().toByteArray());

                if (kvRequest.hasCommand()) {
                    if (kvRequest.getCommand() == Constants.PUT_COMMAND && noPuts) {
                        throw new HeapSpaceWarningException("Memory usage approaching limit.");
                    }
                }

                if (Utilities.validateChecksum(message)) {
                    LOGGER.log(Level.INFO, "UdpClient received a valid packet.");
                    return new RequestInfo(packet.getAddress(), packet.getPort(), message, kvRequest);
                }
            } catch (InvalidProtocolBufferException e) {
                LOGGER.log(Level.INFO, "Protocol Buffer Exception occurred while updClient parsed message.");
            } catch (IncorrectChecksumException e) {
                LOGGER.log(Level.INFO, "UdpClient received a corrupted packet.");
            } catch (PacketLengthZeroException e) {
                LOGGER.log(Level.INFO, "Attempted to extract bytes from a length zero packet.");
            } catch (OutOfMemoryError e) {
                LOGGER.log(Level.SEVERE, "Ran out of heap space.");
                System.exit(1);
            } catch (HeapSpaceWarningException e) {
                LOGGER.log(Level.INFO, "Received put command while no puts specified.");
                KVResponse response = KVResponse.newBuilder().setErrCode(Constants.ERR_CODE_OUT_OF_SPACE).build();
                send(response, message.getMessageID(), packet.getPort(), packet.getAddress());
            }
        }
    }

    /**
     * Sends a message to specified client node.
     *
     * @param keyValueResponse response object to send client
     * @param messageId unique message id
     * @param port port the client is listening on
     * @param inetAddress IPv4 address of the client
     */
    public void send(KVResponse keyValueResponse, ByteString messageId, Integer port, InetAddress inetAddress) {
        try {
            ByteString response = keyValueResponse.toByteString();
            long checksum = makeChecksum(concatenateByteArrays(messageId.toByteArray(), response.toByteArray()));
            byte[] bytes = newMessage(messageId, response, checksum).toByteArray();

            DatagramPacket packet = new DatagramPacket(bytes, bytes.length, inetAddress, port);
            socket.send(packet);
            LOGGER.log(Level.INFO, "UdpClient sent a packet to a client node.");
        } catch (IOException e) {
            LOGGER.log(Level.INFO, "An IOException occurred while sending a packet to client node.");
        }
    }

    /**
     * Sends a message to the server node.
     *
     * @param keyValueRequest request object for the server
     * @param messageId unique id generated for this request
     * @return server reply containing response payload and messageId
     * @throws IOException on socket error
     * @throws RequestReplyTimeoutException if server does not respond in time
     */
    public KVResponse send(KVRequest keyValueRequest, ByteString messageId)
            throws IOException, RequestReplyTimeoutException {
        LOGGER.log(Level.INFO, "UdpClient sending a packet to a server node.");

        ByteString request = keyValueRequest.toByteString();
        long checksum = makeChecksum(concatenateByteArrays(messageId.toByteArray(), keyValueRequest.toByteArray()));
        byte[] bytes = newMessage(messageId, request, checksum).toByteArray();

        DatagramPacket sendPacket = new DatagramPacket(bytes, bytes.length, destinationAddress, destinationPort);

        byte[] receiveBuffer = new byte[Constants.RECEIVE_BUFFER_LENGTH];
        DatagramPacket receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);

        return sendToServer(receivePacket, sendPacket);
    }

    /**
     * Method implements the logic to send a request to the server. This includes retry attempts with
     * the same messageId, and validation of the checksum of any response message.f
     *
     * @param receivePacket packet to receive data in
     * @param sendPacket packet already containing data to send the server
     * @return a message received in response from the server
     * @throws RequestReplyTimeoutException if the server does not respond
     * @throws IOException if there is a problem sending or receiving a packet
     */
    private KVResponse sendToServer(DatagramPacket receivePacket, DatagramPacket sendPacket)
        throws RequestReplyTimeoutException, IOException {

        KVResponse kvResponse = null;

        long startTime = System.currentTimeMillis();
        long elapsedTime = 0L;

        int timeout = this.timeoutMilliseconds;
        this.socket.setSoTimeout(timeout);

        this.socket.send(sendPacket);

        for (int attempts = 0; attempts < Constants.SEND_ATTEMPTS && elapsedTime < Constants.REPLY_TIMEOUT; attempts++) {

            try {
                LOGGER.log(Level.INFO, "Attempting to receive a response from the server.");
                socket.receive(receivePacket);
                if (validateChecksum(Message.Msg.parseFrom(extractPacketBytes(receivePacket)))) {
                    byte[] bytes = new byte[receivePacket.getLength()];
                    System.arraycopy(receivePacket.getData(), 0, bytes, 0, receivePacket.getLength());
                    kvResponse = KVResponse.parseFrom(Message.Msg.parseFrom(bytes).getPayload());
                    break;
                }
            } catch (IncorrectChecksumException e) {
                LOGGER.log(Level.INFO, "UdpClient received a corrupted response message from the server");
            } catch (SocketTimeoutException e) {
                timeout = timeout * (2 ^ (attempts + 1));
                LOGGER.log(Level.INFO, "Receive packet attempt timed out. Increasing timeout value to: " + timeout);
                socket.setSoTimeout(timeout);
            } catch (InvalidProtocolBufferException e) {
                LOGGER.log(Level.INFO, "Protocol Buffer Exception encountered while udpClient parsed bytes.");
            } catch (PacketLengthZeroException e) {
                LOGGER.log(Level.INFO, "Packet length was zero.");
            }

            socket.send(sendPacket);
            elapsedTime = (new Date()).getTime() - startTime;
            attempts++;
        }

        if (kvResponse == null) {
            throw new RequestReplyTimeoutException("Server did not respond");
        }

        return kvResponse;
    }

    private byte[] extractPacketBytes(DatagramPacket packet) throws PacketLengthZeroException {
        int length = packet.getLength();

        if (length == 0) {
            throw new PacketLengthZeroException("Attempted to extract bytes from a packet of length zero.");
        }

        byte[] returnValue = new byte[length];
        System.arraycopy(packet.getData(), 0, returnValue, 0, length);
        return returnValue;
    }

    private Message.Msg newMessage(ByteString id, ByteString payload, long checksum) {
        return Message.Msg.newBuilder()
                .setMessageID(id)
                .setPayload(payload)
                .setCheckSum(checksum)
                .build();
    }

}
