package com.s35723105.CPEN431.A2.CPEN431_A2;

import ca.NetSysLab.ProtocolBuffers.KeyValueRequest.KVRequest;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse;

public interface CommandProcessor {

    public KVResponse processCommand(KVRequest kvRequest);

    public Double getSize();

}
