package com.s35723105.CPEN431.A2.CPEN431_A2;

public class PacketLengthZeroException extends Exception {

    public PacketLengthZeroException(String message) {
        super(message);
    }
}
