package com.s35723105.CPEN431.A2.CPEN431_A2;

public class IncorrectChecksumException extends Exception {

    public IncorrectChecksumException(String message) {
        super(message);
    }
}
