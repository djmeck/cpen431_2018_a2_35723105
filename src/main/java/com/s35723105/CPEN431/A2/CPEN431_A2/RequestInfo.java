package com.s35723105.CPEN431.A2.CPEN431_A2;

import ca.NetSysLab.ProtocolBuffers.KeyValueRequest.KVRequest;
import ca.NetSysLab.ProtocolBuffers.Message;

import java.net.InetAddress;

public class RequestInfo {

    private final InetAddress inetAddress;
    private final Integer port;
    private final Message.Msg message;
    private final KVRequest kvRequest;

    public RequestInfo(InetAddress inetAddress,
                       Integer port,
                       Message.Msg message,
                       KVRequest kvRequest) {

        this.inetAddress = inetAddress;
        this.port = port;
        this.message = message;
        this.kvRequest = kvRequest;
    }

    public Message.Msg getResponseMessage() {
        return this.message;
    }

    public Integer getPort() {
        return this.port;
    }

    public InetAddress getInetAddress() {
        return this.inetAddress;
    }

    public KVRequest getKvRequest() {
        return this.kvRequest;
    }
}
