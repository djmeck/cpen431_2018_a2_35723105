package com.s35723105.CPEN431.A2.CPEN431_A2;

public class Constants {

    private Constants() {}

    /** Upd Client configurations **/

    public static final Integer RECEIVE_BUFFER_LENGTH = 16384;
    public static final Integer SEND_ATTEMPTS = 4;
    public static final Integer MESSAGE_ID_LEN_BYTES = 16;
    public static final Integer REPLY_TIMEOUT = 5000;
    public static final Integer EXPIRE_TIME = 5;
    public static final Integer SOURCE_PORT = 12357;

    /** Key value store and request reply client configurations **/

    public static final Integer INITIAL_CACHE_SIZE = 100;
    public static final Integer INITIAL_STORE_SIZE = 100;
    public static final Integer VERSION_SIZE_BYTES = 4;
    public static final Integer VERSION_ONE = 1;
    public static final Integer MAX_VALUE_SIZE_BYTES = 10000;
    public static final Integer MAX_KEY_SIZE_BYTES = 32;
    public static final Double HEAP_SIZE = 64000000.0;
    public static final Double HEAP_THRESHOLD = 52000000.0;
    public static final Double NO_CHANGE = 0.0;

    /** Acceptable key value store command operations **/

    public static final Integer PUT_COMMAND = 1;
    public static final Integer GET_COMMAND = 2;
    public static final Integer REMOVE_COMMAND = 3;
    public static final Integer SHUTDOWN_COMMAND = 4;
    public static final Integer WIPEOUT_COMMAND = 5;
    public static final Integer ALIVE_COMMAND = 6;
    public static final Integer PID_GET_COMMAND = 7;

    /** Status codes for key value store operations **/

    public static final Integer OPERATION_SUCCESSFUL = 0;

    public static final Integer ERR_CODE_NON_EXISTENT_KEY = 1;
    public static final Integer ERR_CODE_OUT_OF_SPACE = 2;
    public static final Integer ERR_CODE_TEMP_SYS_OVERLOAD = 3;
    public static final Integer ERR_CODE_INTERNAL_KV_FAILURE = 4;
    public static final Integer ERR_CODE_UNRECOGNIZED_COMMAND = 5;
    public static final Integer ERR_CODE_INVALID_KEY = 6;
    public static final Integer ERR_CODE_INVALID_VALUE = 7;

    public static final Integer ERR_CODE_MISSING_PARAMETER = 32;
    public static final Integer ERR_CODE_INVALID_VERSION = 33;
}
