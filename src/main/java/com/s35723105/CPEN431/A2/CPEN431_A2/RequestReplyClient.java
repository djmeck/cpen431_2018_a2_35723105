package com.s35723105.CPEN431.A2.CPEN431_A2;

import ca.NetSysLab.ProtocolBuffers.KeyValueRequest.KVRequest;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.protobuf.ByteString;
import net.jodah.expiringmap.ExpiringMap;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.s35723105.CPEN431.A2.CPEN431_A2.Utilities.newMessageId;

public class RequestReplyClient {

    private final static Logger LOGGER = Logger.getLogger(RequestReplyClient.class.getName());

    private final UdpClient udpClient;

    private Map<String, KVResponse> cache;

    /**
     * Constructs a updClient for receiving bytes from clients and sending them responses.
     * Destination port and address are therefore not required, as this will depend on the client
     * being responded to. Source port is of course necessary.
     *
     * @param sourcePort the source port the server should listen on
     * @throws IOException if ip address provided is invalid
     */
    RequestReplyClient(Integer sourcePort) throws IOException{
        LOGGER.log(Level.INFO, "Creating a request reply client.");
        this.udpClient = new UdpClient(sourcePort);
        initializeCache();
    }

    /**
     * Constructs a updClient for sending and receiving bytes to a specified host
     *
     * @param destinationIpAddress ip address of the destination host in IPv4 format
     * @param destinationPort port the destination host is listening on
     * @throws IOException if ip address provided is invalid
     */
    RequestReplyClient(String destinationIpAddress, Integer destinationPort) throws IOException {
        LOGGER.log(Level.INFO, "Creating a request reply client.");
        this.udpClient = new UdpClient(destinationPort, destinationIpAddress);
        cache = null;
    }

    /**
     * Constructs a updClient for sending and receving bytes to a specified host.
     *
     * @param destinationIpAddress ip address of the destination host in IPv4 format
     * @param destinationPort port the destination host is listening on
     * @param timeout waiting period for response which doubles for successive attempts
     * @throws IOException if ip address provided is invalid
     */
    RequestReplyClient(String destinationIpAddress, Integer destinationPort, Integer timeout)
            throws IOException {
        LOGGER.log(Level.INFO, "Creating a request reply client.");
        this.udpClient = new UdpClient(destinationPort, destinationIpAddress, timeout);
        cache = null;
    }

    /**
     * Initializes the loader cache. Elements in the loading cache expire after an
     * amount of time which is configurable in the Constants file. As per the instructions
     * in the assignment, this value should be 5 seconds.
     *
     * The loading cache accepts a messages unique id as the key. The value should
     * be a byte array with a single value (1) in it.
     */
    private void initializeCache() {
        LOGGER.log(Level.INFO, "Initializing the cache of a request reply client.");
        this.cache = ExpiringMap.builder().expiration(Constants.EXPIRE_TIME, TimeUnit.SECONDS).build();
    }

    /**
     * Method will listen for incoming packets until one arrives --if the packet was invalid
     * for some reason, it will not be returned, and the method will continue listening.
     *
     * @param noPuts if put operations should be ignored
     * @return Response object containing information about the requesting client
     * @throws IOException if a problem occurred while receiving on the socket
     */
    public RequestInfo listen(Boolean noPuts) throws IOException {
        while (true) {
            RequestInfo requestInfo = udpClient.listen(noPuts);
            LOGGER.log(Level.INFO, "Request reply client received a message.");
            String key = Utilities.byteArrayToHexString(requestInfo.getResponseMessage().getMessageID().toByteArray());
            KVResponse entry = cache.get(key);
                if (entry == null) {
                return requestInfo;
            } else {
                respondToClientNode(entry, requestInfo.getResponseMessage().getMessageID(),
                        requestInfo.getInetAddress(), requestInfo.getPort());
            }
            LOGGER.log(Level.INFO, "Responded to a duplicate request with a cache value.");

        }
    }

    /**
     * Sends a response to a client node.
     *
     * @param response response message to send the client
     * @param messageId message id of the request being responded to
     * @param inetAddress IPv4 address of the client node
     * @param port port that the client is listening on
     */
    public void respondToClientNode(KVResponse response, ByteString messageId, InetAddress inetAddress, Integer port) {
        LOGGER.log(Level.INFO, "Request reply client sending response a client node.");
        cache.put(Utilities.byteArrayToHexString(messageId.toByteArray()), response);
        this.udpClient.send(response, messageId, port, inetAddress);
    }

    /**
     * Method will send a request to the server.
     *
     * @param command indicates the action to be taken by the server
     * @param key the key for the key value store
     * @param value the value corresponding to the key
     * @param version being requested from the server
     * @return response from the server
     * @throws IOException if a networking problem occurs with the socket
     */
    public KVResponse newRequest(Integer command, ByteString key, ByteString value, Integer version)
            throws IOException {
        LOGGER.log(Level.INFO, "Creating a request to send to the server node.");
        KVResponse returnValue = null;
        KVRequest kvRequest = KVRequest.newBuilder()
                .setCommand(command)
                .setKey(key)
                .setValue(value)
                .setVersion(version)
                .build();

        try {
            returnValue = this.udpClient.send(kvRequest, newMessageId());
        } catch (RequestReplyTimeoutException e) {
            LOGGER.log(Level.INFO, "A request sent to the server node timed out.");
        }

        return returnValue;
    }

    /**
     * Method will send a request to the server.
     *
     * @param command indicates the action to be taken by the server
     * @param key the key for the key value store
     * @param value the value corresponding to the key
     * @return response from the server
     * @throws IOException if a networking problem occurs with the socket
     */
    public KVResponse newRequest(Integer command, ByteString key, ByteString value)
            throws IOException {
        LOGGER.log(Level.INFO, "Creating a request to send to the server node.");
        KVResponse returnValue = null;
        KVRequest kvRequest = KVRequest.newBuilder()
                .setCommand(command)
                .setKey(key)
                .setValue(value)
                .build();

        try {
            returnValue = this.udpClient.send(kvRequest, newMessageId());
        } catch (RequestReplyTimeoutException e) {
            LOGGER.log(Level.INFO, "A request sent to the server node timed out.");
        }

        return returnValue;
    }

    /**
     * Method will send a request to the server.
     *
     * @param command indicates the action to be taken by the server
     * @param key the key for the key value store
     * @return response from the server
     * @throws IOException if a networking problem occurs with the socket
     */
    public KVResponse newRequest(Integer command, ByteString key)
            throws IOException {
        LOGGER.log(Level.INFO, "Creating a request to send to the server node.");
        KVResponse returnValue = null;
        KVRequest kvRequest = KVRequest.newBuilder()
                .setCommand(command)
                .setKey(key)
                .build();

        try {
            returnValue = this.udpClient.send(kvRequest, newMessageId());
        } catch (RequestReplyTimeoutException e) {
            LOGGER.log(Level.INFO, "A request sent to the server node timed out.");
        }

        return returnValue;
    }

    /**
     * Method will send a request to the server. Only the command in specified. This method
     * is appropriate for actions like shutting down the server.
     *
     * @param command command for the server
     * @return response object from the server
     * @throws IOException if networking problem with the socket
     */
    public KVResponse newRequest(Integer command) throws IOException {
        LOGGER.log(Level.INFO, "Creating a request to send to the server node.");
        KVResponse returnValue = null;

        KVRequest kvRequest = KVRequest.newBuilder()
                .setCommand(command)
                .build();

        try {
            returnValue = this.udpClient.send(kvRequest, newMessageId());
        } catch (RequestReplyTimeoutException e) {
            LOGGER.log(Level.INFO, "A request sent to the server node timed out.");
        }

        return returnValue;
    }

}
