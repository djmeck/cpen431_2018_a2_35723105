package com.s35723105.CPEN431.A2.CPEN431_A2;

import com.google.protobuf.ByteString;

import java.nio.ByteBuffer;

public class Element {

    private byte[] version = new byte[Constants.VERSION_SIZE_BYTES];
    private ByteString value;

    public Element(Integer version, ByteString value) {
        this.value = value;
        this.version = ByteBuffer.allocate(Constants.VERSION_SIZE_BYTES).putInt(version).array();
    }

    public Integer getVersion() {
        return ByteBuffer.wrap(this.version).getInt();
    }

    public ByteString getValue() {
        return this.value;
    }

    public void setVersion(Integer newVersion) {
        this.version = ByteBuffer.allocate(Constants.VERSION_SIZE_BYTES).putInt(newVersion).array();
    }

    public void setValue(ByteString newValue) {
        this.value = newValue;
    }
}
