package com.s35723105.CPEN431.A2.CPEN431_A2;

public class HeapSpaceWarningException extends Exception {

    public HeapSpaceWarningException(String message) {
        super(message);
    }
}
