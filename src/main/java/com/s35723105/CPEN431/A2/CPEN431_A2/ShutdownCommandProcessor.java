package com.s35723105.CPEN431.A2.CPEN431_A2;

import ca.NetSysLab.ProtocolBuffers.KeyValueRequest;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse;

import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ShutdownCommandProcessor implements CommandProcessor {

    private final static Logger LOGGER = Logger.getLogger(ShutdownCommandProcessor.class.getName());
    private final ConcurrentHashMap<String, Element> store;

    public ShutdownCommandProcessor(ConcurrentHashMap<String, Element> store) {
        this.store = store;
    }

    public KVResponse processCommand(KeyValueRequest.KVRequest kvRequest) {

        LOGGER.log(Level.INFO, "Shutting down the server.");

        System.exit(0);

        return KVResponse.newBuilder().build();
    }

    public Double getSize() {
        return -1.0;
    }
}
