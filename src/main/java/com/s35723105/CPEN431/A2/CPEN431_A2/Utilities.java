package com.s35723105.CPEN431.A2.CPEN431_A2;

import ca.NetSysLab.ProtocolBuffers.Message;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;

import java.util.concurrent.ThreadLocalRandom;
import java.util.zip.CRC32;

import org.apache.commons.codec.binary.Hex;

public class Utilities {

    private Utilities() {}

    public static boolean validateChecksum(Message.Msg message)
            throws IncorrectChecksumException, InvalidProtocolBufferException {
        long cs = makeChecksum(concatenateByteArrays(message.getMessageID().toByteArray(),
                message.getPayload().toByteArray()));
        return cs == message.getCheckSum();
    }

    public static long makeChecksum(byte[] bytes) {
        CRC32 crc32 = new CRC32();
        crc32.update(bytes);
        return crc32.getValue();
    }

    public static byte[] concatenateByteArrays(byte[] messageId, byte[] payload) {
        byte[] returnValue = new byte[messageId.length + payload.length];
        System.arraycopy(messageId, 0, returnValue, 0, messageId.length);
        System.arraycopy(payload, 0, returnValue, messageId.length, payload.length);
        return returnValue;
    }

    public static ByteString newMessageId() {
        byte[] returnValue = new byte[Constants.MESSAGE_ID_LEN_BYTES];
        for (int i = 0; i < Constants.MESSAGE_ID_LEN_BYTES; i++) {
            int nextByte = (byte) ThreadLocalRandom.current().nextInt(0, 256);
            returnValue[i] = (byte) nextByte;
        }
        return com.google.protobuf.ByteString.copyFrom(returnValue);
    }

    public static String byteArrayToHexString(byte[] bytes) {
        String returnValue = new String(Hex.encodeHex(bytes));
        return returnValue;
    }

}
