package com.s35723105.CPEN431.A2.CPEN431_A2;

public class CannotParsePidException extends Exception {

    public CannotParsePidException(String message) {
        super(message);
    }

}
