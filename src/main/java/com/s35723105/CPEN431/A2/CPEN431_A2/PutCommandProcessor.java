package com.s35723105.CPEN431.A2.CPEN431_A2;

import ca.NetSysLab.ProtocolBuffers.KeyValueRequest.KVRequest;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse;
import com.google.protobuf.ByteString;

import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PutCommandProcessor implements CommandProcessor {

    private final static Logger LOGGER = Logger.getLogger(PutCommandProcessor.class.getName());
    private final ConcurrentHashMap<String, Element> store;

    private Double size;

    public PutCommandProcessor(ConcurrentHashMap<String, Element> store) {
        this.store = store;
        this.size = 0.0;
    }

    public KVResponse processCommand(KVRequest kvRequest) {

        if (!kvRequest.hasValue() || !kvRequest.hasKey()) {
            LOGGER.log(Level.INFO, "A put request was missing a value or key parameter.");
            return KVResponse.newBuilder().setErrCode(Constants.ERR_CODE_MISSING_PARAMETER).build();
        }

        byte[] key = kvRequest.getKey().toByteArray();
        if (key.length > Constants.MAX_KEY_SIZE_BYTES) {
            LOGGER.log(Level.INFO, "A put request key exceeded the maximum allowable size.");
            return KVResponse.newBuilder().setErrCode(Constants.ERR_CODE_INVALID_KEY).build();
        }

        ByteString value = kvRequest.getValue();
        if(value.size() > Constants.MAX_VALUE_SIZE_BYTES) {
            LOGGER.log(Level.INFO, "A put request value exceeded the maximum allowable size.");
            return KVResponse.newBuilder().setErrCode(Constants.ERR_CODE_INVALID_VALUE).build();
        }

        LOGGER.log(Level.INFO, "Inserting element into store with key: " + Utilities.byteArrayToHexString(key));

        String kStr = Utilities.byteArrayToHexString(key);
        if (store.containsKey(kStr)) {
            Element element = store.get(kStr);
            Integer version = element.getVersion();
            version = (kvRequest.hasVersion()) ? kvRequest.getVersion() : version++;
            element.setVersion(version);
            element.setValue(value);
            store.put(kStr, element);
        } else {
            Integer version = (kvRequest.hasVersion()) ? kvRequest.getVersion() : Constants.VERSION_ONE;
            Element element = new Element(version, value);
            store.put(kStr, element);
        }

        size = key.length / (1.0) + value.size() / (1.0);

        LOGGER.log(Level.INFO, "Successfully completed a put operation.");
        return KVResponse.newBuilder().setErrCode(Constants.OPERATION_SUCCESSFUL).build();
    }

    public Double getSize() {
        return size;
    }
}
