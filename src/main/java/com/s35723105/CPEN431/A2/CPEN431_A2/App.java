package com.s35723105.CPEN431.A2.CPEN431_A2;

import ca.NetSysLab.ProtocolBuffers.KeyValueResponse;
import com.google.protobuf.ByteString;

import java.io.IOException;
import java.net.InetAddress;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.LogManager;

public class App 
{
    public static void main( String[] args )
    {
        try {
            int arg = Integer.parseInt(args[0]);
            if (arg == 1) {
                InetAddress inetAddress = InetAddress.getLocalHost();
                System.out.println("Expecting server to be at: " + inetAddress.toString());
                RequestReplyClient client = new RequestReplyClient(inetAddress.getHostAddress(), Constants.SOURCE_PORT, 300);
                ByteString key = randomByteString(Constants.MAX_KEY_SIZE_BYTES);
                ByteString value = randomByteString(100);
                KeyValueResponse.KVResponse response = client.newRequest(Constants.PUT_COMMAND, key, value);
                System.out.println("Status code from put: " + response.getErrCode());
                System.out.println("What was put: " + Utilities.byteArrayToHexString(value.toByteArray()));
                Thread.sleep(3000);
                response = client.newRequest(Constants.GET_COMMAND, key);
                System.out.println("Status code from get request: " + response.getErrCode());
                System.out.println("What was returned: " + Utilities.byteArrayToHexString(response.getValue().toByteArray()));
            } else {
                LogManager.getLogManager().reset();
                KeyValueStore keyValueStore = new KeyValueStore();
                keyValueStore.listen();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }

    private static ByteString randomByteString(Integer maxSize) {
        int length = ThreadLocalRandom.current().nextInt(0, maxSize);
        byte[] returnValue = new byte[length];
        for (int i = 0; i < length; i++) {
            returnValue[i] = (byte) ThreadLocalRandom.current().nextInt(0, 256);
        }
        return ByteString.copyFrom(returnValue);
    }
}
