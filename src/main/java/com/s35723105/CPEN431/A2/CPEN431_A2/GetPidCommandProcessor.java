package com.s35723105.CPEN431.A2.CPEN431_A2;

import ca.NetSysLab.ProtocolBuffers.KeyValueRequest.KVRequest;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse;

import java.lang.management.ManagementFactory;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GetPidCommandProcessor implements CommandProcessor {

    private final static Logger LOGGER = Logger.getLogger(GetPidCommandProcessor.class.getName());
    private final ConcurrentHashMap<String, Element> store;

    public GetPidCommandProcessor(ConcurrentHashMap<String, Element> store) {
        this.store = store;
    }

    public KVResponse processCommand(KVRequest kvRequest) {
        KVResponse response;

        try {
            Integer pid = Integer.parseInt(ManagementFactory.getRuntimeMXBean().getName());
            LOGGER.log(Level.INFO, "Retrieved the following process id: " + pid);
            response = KVResponse.newBuilder()
                    .setErrCode(Constants.OPERATION_SUCCESSFUL)
                    .setPid(pid)
                    .build();

        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Unable to parse process id.");
            response = KVResponse.newBuilder()
                    .setErrCode(Constants.ERR_CODE_INTERNAL_KV_FAILURE)
                    .build();
        }

        return response;
    }

    public Double getSize() {
        return Constants.NO_CHANGE;
    }
}
