package com.s35723105.CPEN431.A2.CPEN431_A2;

import ca.NetSysLab.ProtocolBuffers.KeyValueRequest.KVRequest;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse;

import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RequestHandler implements Runnable {

    private final RequestInfo requestInfo;
    private final KeyValueStore keyValueStore;

    private final static Logger LOGGER = Logger.getLogger(RequestHandler.class.getName());

    public RequestHandler(RequestInfo operandOne,
                          KeyValueStore operandThree) {

        this.requestInfo = operandOne;
        this.keyValueStore = operandThree;
    }

    public void run() {
        KVRequest request = requestInfo.getKvRequest();
        CommandProcessor commandProcessor = keyValueStore.commandProcessorMap.get(request.getCommand());
        if (commandProcessor == null) {
            LOGGER.log(Level.INFO, "A request containing an unknown command was received.");
            KVResponse response = KVResponse.newBuilder().setErrCode(Constants.ERR_CODE_UNRECOGNIZED_COMMAND).build();
            keyValueStore.respondToClient(response, requestInfo.getResponseMessage().getMessageID(),
                    requestInfo.getInetAddress(), requestInfo.getPort(), Constants.NO_CHANGE);
        } else {
            KVResponse response = commandProcessor.processCommand(request);
            keyValueStore.respondToClient(response, requestInfo.getResponseMessage().getMessageID(),
                    requestInfo.getInetAddress(), requestInfo.getPort(), commandProcessor.getSize());
        }
    }

}
