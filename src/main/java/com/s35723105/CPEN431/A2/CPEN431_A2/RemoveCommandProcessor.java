package com.s35723105.CPEN431.A2.CPEN431_A2;

import ca.NetSysLab.ProtocolBuffers.KeyValueRequest.KVRequest;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse;

import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RemoveCommandProcessor implements CommandProcessor {

    private final static Logger LOGGER = Logger.getLogger(RemoveCommandProcessor.class.getName());
    private final ConcurrentHashMap<String, Element> store;

    private Double size;

    public RemoveCommandProcessor(ConcurrentHashMap<String, Element> store) {
        this.store = store;
        this.size = 0.0;
    }

    public KVResponse processCommand(KVRequest kvRequest) {

        if (!kvRequest.hasKey()) {
            LOGGER.log(Level.INFO, "Received remove request without a key specified.");
            return KVResponse.newBuilder().setErrCode(Constants.ERR_CODE_MISSING_PARAMETER).build();
        }

        byte[] key = kvRequest.getKey().toByteArray();
        String kStr = Utilities.byteArrayToHexString(key);
        if (!store.containsKey(kStr)) {
            LOGGER.log(Level.INFO, "Received remove request for non-existent key.");
            return KVResponse.newBuilder().setErrCode(Constants.ERR_CODE_NON_EXISTENT_KEY).build();
        }

        LOGGER.log(Level.INFO, "Removing the following key from the store: " + kStr);

        size += store.get(kStr).getValue().size() / (-1.0);
        size += key.length / (-1.0);

        store.remove(kStr);

        return KVResponse.newBuilder().setErrCode(Constants.OPERATION_SUCCESSFUL).build();
    }

    public Double getSize() {
        return size;
    }

}
