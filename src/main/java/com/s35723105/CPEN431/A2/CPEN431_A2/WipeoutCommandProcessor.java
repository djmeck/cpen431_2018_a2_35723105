package com.s35723105.CPEN431.A2.CPEN431_A2;

import ca.NetSysLab.ProtocolBuffers.KeyValueRequest.KVRequest;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse;

import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WipeoutCommandProcessor implements CommandProcessor {

    private final static Logger LOGGER = Logger.getLogger(WipeoutCommandProcessor.class.getName());
    private final ConcurrentHashMap<String, Element> store;

    public WipeoutCommandProcessor(ConcurrentHashMap<String, Element> store) {
        this.store = store;
    }

    public KVResponse processCommand(KVRequest kvRequest) {

        LOGGER.log(Level.INFO, "Deleting all entries in the key value store.");

        store.clear();

        return KVResponse.newBuilder().setErrCode(Constants.OPERATION_SUCCESSFUL).build();
    }

    public Double getSize() {
        return -1.0;
    }
}
