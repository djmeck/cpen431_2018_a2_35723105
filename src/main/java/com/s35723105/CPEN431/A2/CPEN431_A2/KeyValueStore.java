package com.s35723105.CPEN431.A2.CPEN431_A2;

import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse;
import com.google.protobuf.ByteString;

import java.io.IOException;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class KeyValueStore {

    private final static Logger LOGGER = Logger.getLogger(KeyValueStore.class.getName());

    private final ConcurrentHashMap<String, Element> store;
    private final RequestReplyClient requestReplyClient;

    private final ExecutorService executorService = Executors.newFixedThreadPool(16);

    private Double estimatedTotalSize = 0.0;
    private Double estimatedAverageSize = 0.0;
    private final Object lock = new Object();

    private Boolean noPuts = false;

    public final Map<Integer, CommandProcessor> commandProcessorMap = new HashMap<>();

    public KeyValueStore() throws IOException {
        LOGGER.log(Level.INFO, "Creating a key value store.");
        this.store = new ConcurrentHashMap<>(Constants.INITIAL_STORE_SIZE);
        this.noPuts = false;
        requestReplyClient = new RequestReplyClient(Constants.SOURCE_PORT);
        initializeCommandProcessorMap();
    }

    public void listen() throws IOException {
        while (true) {
            Double estimate = getEstimatedTotalSize();
            noPuts = estimate >= Constants.HEAP_THRESHOLD;
            RequestInfo requestInfo = requestReplyClient.listen(noPuts);
            LOGGER.log(Level.INFO, "Key value store creating thread to handle incoming request.");
            executorService.execute(new RequestHandler(requestInfo, this));
        }
    }

    public void respondToClient(KVResponse kvResponse, ByteString messageId, InetAddress inetAddress,
                                Integer port, Double size) {
        if (size == -1.0) {
            resetStatistics();
        } else {
            updateUsageStatistics(size);
        }
        Double estSize = getEstimatedTotalSize();
        Double avgSize = getEstimatedAverageSize();
        LOGGER.log(Level.INFO, "Estimated memory usage of store: " + estSize + " bytes.");
        LOGGER.log(Level.INFO, "Estimated average size of entry: " + avgSize + " bytes.");
        this.requestReplyClient.respondToClientNode(kvResponse, messageId, inetAddress, port);
        LOGGER.log(Level.INFO, "Key value store sending response to client.");
    }

    private void initializeCommandProcessorMap() {
        LOGGER.log(Level.INFO, "Initializing the key value store's command processor map.");
        this.commandProcessorMap.put(Constants.PUT_COMMAND, new PutCommandProcessor(store));
        this.commandProcessorMap.put(Constants.GET_COMMAND, new GetCommandProcessor(store));
        this.commandProcessorMap.put(Constants.REMOVE_COMMAND, new RemoveCommandProcessor(store));
        this.commandProcessorMap.put(Constants.ALIVE_COMMAND, new AliveCommandProcessor(store));
        this.commandProcessorMap.put(Constants.WIPEOUT_COMMAND, new WipeoutCommandProcessor(store));
        this.commandProcessorMap.put(Constants.SHUTDOWN_COMMAND, new ShutdownCommandProcessor(store));
        this.commandProcessorMap.put(Constants.PID_GET_COMMAND, new GetPidCommandProcessor(store));
    }

    public void updateUsageStatistics(Double value) {
        synchronized (lock) {
            if (value == -1.0) {
                estimatedAverageSize = 0.0;
                estimatedTotalSize = 0.0;
            } else {
                estimatedTotalSize += value;
                estimatedAverageSize = estimatedTotalSize / store.size();
            }
        }
    }

    public Double getEstimatedAverageSize() {
        synchronized (lock) {
            return estimatedAverageSize;
        }
    }

    public Double getEstimatedTotalSize() {
        synchronized (lock) {
            return estimatedTotalSize;
        }
    }

    public void resetStatistics() {
        synchronized (lock) {
            this.estimatedTotalSize = 0.0;
            this.estimatedAverageSize = 0.0;
        }
    }

}
