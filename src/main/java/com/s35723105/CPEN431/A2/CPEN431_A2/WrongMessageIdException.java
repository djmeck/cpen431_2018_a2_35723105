package com.s35723105.CPEN431.A2.CPEN431_A2;

public class WrongMessageIdException extends Exception {
    public WrongMessageIdException(String message) {
        super(message);
    }
}
