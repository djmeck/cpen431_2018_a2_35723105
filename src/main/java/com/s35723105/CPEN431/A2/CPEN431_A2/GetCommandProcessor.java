package com.s35723105.CPEN431.A2.CPEN431_A2;

import ca.NetSysLab.ProtocolBuffers.KeyValueRequest.KVRequest;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse;

import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.google.protobuf.ByteString.copyFrom;

public class GetCommandProcessor implements CommandProcessor {

    private final static Logger LOGGER = Logger.getLogger(GetCommandProcessor.class.getName());
    private final ConcurrentHashMap<String, Element> store;

    public GetCommandProcessor(ConcurrentHashMap<String, Element> store) {
        this.store = store;
    }

    public KVResponse processCommand(KVRequest kvRequest) {

        if (!kvRequest.hasKey()) {
            LOGGER.log(Level.INFO, "A get request did not contain a key.");
            return KVResponse.newBuilder().setErrCode(Constants.ERR_CODE_MISSING_PARAMETER).build();
        }

        byte[] key = kvRequest.getKey().toByteArray();
        LOGGER.log(Level.INFO, "Attempting get request with key: " + Utilities.byteArrayToHexString(key));

        if (key.length > Constants.MAX_KEY_SIZE_BYTES) {
            LOGGER.log(Level.INFO, "The get request key exceeded the maximum allowable size.");
            return KVResponse.newBuilder().setErrCode(Constants.ERR_CODE_INVALID_KEY).build();
        }

        Element element = store.get(Utilities.byteArrayToHexString(key));
        if (element == null) {
            LOGGER.log(Level.INFO, "The get request key was non-existent.");
            return KVResponse.newBuilder().setErrCode(Constants.ERR_CODE_NON_EXISTENT_KEY).build();
        }

        if (kvRequest.hasVersion()) {
            Integer requestedVersion = kvRequest.getVersion();
            Integer actualVersion = element.getVersion();

            if (requestedVersion != actualVersion) {
                LOGGER.log(Level.INFO, "Get request was made for an invalid version.");
                return KVResponse.newBuilder().setErrCode(Constants.ERR_CODE_INVALID_VERSION).build();
            }
        }

        LOGGER.log(Level.INFO, "Successfully completed a get operation.");

        return KVResponse.newBuilder()
                .setErrCode(Constants.OPERATION_SUCCESSFUL)
                .setValue(element.getValue())
                .setVersion(element.getVersion())
                .build();
    }

    public Double getSize() {
        return Constants.NO_CHANGE;
    }

}
