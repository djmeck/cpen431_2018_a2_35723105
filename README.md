##Name: Devin Meckling
##Student Number: 35723105

###To run the server:

```
java -Xmx64m -jar A4.jar 0
```

###Extra error codes:

0x20: parameter missing --for example, a put with a key but no value

0X21: invalid version --version requested does not match version in store

###Design choices:

1) Server is a multithreaded implementation. For A4, I modified the number of threads to be fixed at 2.

2) UdpClient handles all socket communication, abstracts away the retry logic for client nodes, 
and discards corrupted packets.
    
3) ReplyRequestClient manages the cache, implementing at-most-once semantics.
    
4) KeyValueStore spawns threads to handle requests and manages memory usage.

5) Command processing follows a polymorphic pattern --one interface gets implemented for every command, 
and a runnable request handler calls those. This runnable gets submitted to an Executor. The thread also 
responds to the client.

###Tests:

Tests did not focus so much on load testing, but I did test dozens of permutations
of put, get and delete, so that all corner cases were covered.